#include "GameObjectFactory.hpp"

#include "NPC.hpp"
#include "Rocket.hpp"

#include <cassert>

GameObjectFactory::GameObjectFactory()
{

}

GameObjectFactory::~GameObjectFactory()
{

}

/* static */ 
GameObject* GameObjectFactory::create(GameObject::Type eType)
{
    switch(eType)
    {
        case GameObject::Type::NPC: 
            return new NPC();
            break;
        case GameObject::Type::ROCKET:
            return new Rocket();
            break;
        default:
            assert(!"GameObjectFactory::Create(): unknown Type");
            break;
    }
    return nullptr;
}
