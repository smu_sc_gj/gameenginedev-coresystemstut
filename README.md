# README #

## Build Instructions ##

This repo is a little different from the simple ones we've looked at last week.  Here the top level ```CMakeList.txt``` sets up the development environment (a Solution in Visual Studio) then adds build targets (Projects in Visual Studio).  As before create a ```build``` directory and build environment before using this to compile the examples. 

## Content ##

Some game patterns common in computer games, these are based on [Object Orientated Game Development](https://www.amazon.co.uk/Object-Oriented-Game-Development-Julian-Gold/dp/032117660X) by Julian Gold.

The projects are:

  * Singleton
  * Factory
  * Better Factory
  * Even Better Factory
  * String Hasher

The first demonstrates the creation of a [singleton](https://en.wikipedia.org/wiki/Singleton_pattern) while the others show improving methods of implementing a [factory](https://en.wikipedia.org/wiki/Factory_method_pattern) pattern in C++. The last project is a String Hasher example. 

## Factory ##

Simple factory pattern, uses a enum to identify the type of class to be created.  

## Better Factory ##

The dependency on an enum is a problem, classes who use the factory need to know about the enum.  Better to use a string so we can just give a the factory the name of the class to be created.  

## Even Better Factory ##

An improved version which allows us to register classes to be created by the factory by providing the name of the class and a pointer to the allocator.  The factory itself is a singleton, to ensure there's not more than one factory creating things.  We could use this to insert an ID in the class.  

## Allocator ##

Created a stack allocator based on Gregory's [book](https://metsearch.cardiffmet.ac.uk/primo-explore/fulldisplay?docid=44CMU_ALMA5154507530002425&context=L&vid=44WHELF_CMU_NUI1&search_scope=CSCOP_EVERYTHING&isFrbr=true&tab=tab1&lang=en_US).

## String Hasher ##

An implementation of the string hasher featured in [C Programming](https://metsearch.cardiffmet.ac.uk/primo-explore/fulldisplay?docid=44CMU_ALMA2118472560002425&context=L&vid=44WHELF_CMU_NUI1&search_scope=CSCOP_EVERYTHING&tab=tab1&lang=en_US) by K & R, this version has been badly ported to C++ and sits inside a singleton. 

## Standard Hasher (Not done) ##

A string hasher based on the C++11 libraries.
