#include "GameObjectFactory.hpp"

#include "NPC.hpp"
#include "Rocket.hpp"

#include <cassert>
#include <cstring>
#include <cstdio>

GameObjectFactory::GameObjectFactory()
{

}

GameObjectFactory::~GameObjectFactory()
{

}

/* static */ 
GameObject* GameObjectFactory::create(const char* pType)
{
    if(!compareStrings(pType,"npc"))
    {
        return new NPC();
    }
    else
    {        
        if(!compareStrings(pType,"rocket"))
        {
            return new Rocket();
        }
        else
        {
            assert(!"GameObjectFactory::Create(): unknown Type");
        }
    }

    return nullptr;
}

bool GameObjectFactory::compareStrings(const char* str1, const char* str2)
{
    /* The book uses strcmpi here, this appears to be some kind of windows only 
       method.  I tried to avoid this with strcasecmp a POSIX and BSD standard from 2001 
       
       Updated:  This does not work on Windows, this is the work around. 
    */

    #ifdef _WIN32
        return strcmpi(str1,str2);
    #else
        return strcasecmp(str1,str2);
    #endif
}
